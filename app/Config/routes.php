<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::parseExtensions('xml');

/* Routes du controller Articles***/
	Router::connect('/blog', array('controller' => 'Articles', 'action' => 'index'));
	Router::connect('/blog/:slug-:id', array('controller' => 'Articles', 'action' => 'details'), array('pass' => array('slug', 'id')));
	Router::connect('/gestion-blog', array('controller' => 'Articles', 'action' => 'liste'));
	Router::connect('/gestion-blog/ecrire-un-article', array('controller' => 'Articles', 'action' => 'editer'));
	Router::connect('/gestion-blog/ecrire-un-article-:id', array('controller' => 'Articles', 'action' => 'editer'), array('pass' => array('id')));
	Router::connect('/gestion-blog/editer/:slug-:id', array('controller' => 'Articles', 'action' => 'editer'), array('pass' => array('slug', 'id')));

/* Routes du controller Categories***/
	Router::connect('/gestion-categories', array('controller' => 'Categories', 'action' => 'liste'));
	Router::connect('/gestion-categories/ajouter', array('controller' => 'Categories', 'action' => 'ajouter'));
	Router::connect('/gestion-categories/editer/:slug-:id', array('controller' => 'Categories', 'action' => 'editer'), array('pass' => array('slug', 'id')));

/* Routes du controller Cepages***/
	Router::connect('/gestion-cepages', array('controller' => 'Cepages', 'action' => 'liste'));
	Router::connect('/gestion-cepages/ajouter', array('controller' => 'Cepages', 'action' => 'ajouter'));
	Router::connect('/gestion-cepages/editer/:slug-:id', array('controller' => 'Cepages', 'action' => 'editer'), array('pass' => array('slug', 'id')));

/* Routes du controller Commandes***/
	Router::connect('/commander', array('controller' => 'Commandes', 'action' => 'commander'));
	Router::connect('/confirmer', array('controller' => 'Commandes', 'action' => 'confirmer'));
	Router::connect('/mes-commandes', array('controller' => 'Commandes', 'action' => 'mes_commandes'));
	Router::connect('/gestion-commandes', array('controller' => 'Commandes', 'action' => 'listing'));
	Router::connect('/paiement/:num_commande-:id_commande', array('controller' => 'Commandes', 'action' => 'paiement'), array('pass' => array('id_commande', 'num_commande')));
	Router::connect('/details/:id', array('controller' => 'Commandes', 'action' => 'details'), array('pass' => array('id')));

/* Routes du controller Commandes***/
	Router::connect('/mes-favoris', array('controller' => 'Favoris', 'action' => 'mes_favoris'));
	Router::connect('/ajouter-aux-favoris-:id', array('controller' => 'Favoris', 'action' => 'ajouter'), array('pass' => array('id')));


/*Routes du controller Pages***/
	Router::connect('/pages/*', array('controller' => 'Pages', 'action' => 'display'));
	Router::connect('/contact', array('controller' => 'Pages', 'action' => 'contact'));
	Router::connect('/degustation', array('controller' => 'Pages', 'action' => 'degustation'));
	Router::connect('/avantages-restaurateur', array('controller' => 'Pages', 'action' => 'avantages_pro'));
	Router::connect('/a-propos', array('controller' => 'Pages', 'action' => 'a_propos'));
	Router::connect('/editer-le-texte-:id', array('controller' => 'Pages', 'action' => 'editer_texte'), array('pass' => array('id')));
	Router::connect('/sitemap', array('controller' => 'Pages', 'action' => 'sitemap'/*, 'ext' => 'xml'*/));
	Router::connect('/conditions-generales-de-vente', array('controller' => 'Pages', 'action' => 'conditions_generales_vente'));



/*Routes du controller Packs***/
	Router::connect('/nos-offres', array('controller' => 'Packs', 'action' => 'offres'));
	Router::connect('/nos-offres/details/:slug-:id', array('controller' => 'Packs', 'action' => 'details'), array('pass' => array('slug', 'id')));
	Router::connect('/nos-offres/:slug', array('controller' => 'Packs', 'action' => 'details'), array('pass' => array('slug')));
	Router::connect('/gestion-packs', array('controller' => 'Packs', 'action' => 'liste'));
	Router::connect('/gestion-packs/ajouter', array('controller' => 'Packs', 'action' => 'ajouter'));
	Router::connect('/gestion-packs/editer/:slug-:id', array('controller' => 'Packs', 'action' => 'editer'), array('pass' => array('slug', 'id')));
	Router::connect('/gestion-packs/:slug-:id', array('controller' => 'Packs', 'action' => 'gerer'), array('pass' => array('slug', 'id')));

/*Routes du controller Paiements**/
	Router::connect('/succes-paypal', array('controller' => 'Paiements', 'action' => 'success_paypal'));
	Router::connect('/succes-postfinance', array('controller' => 'Paiements', 'action' => 'success_poste'));

/*Routes du controller Producteurs***/
	Router::connect('/nos-producteurs', array('controller' => 'Producteurs', 'action' => 'index'));
	Router::connect('/nos-producteurs/:slug', array('controller' => 'Producteurs', 'action' => 'details'), array('pass' => array('slug')));
	Router::connect('/gestion-producteurs', array('controller' => 'Producteurs', 'action' => 'liste'));
	Router::connect('/gestion-producteurs/ajouter', array('controller' => 'Producteurs', 'action' => 'ajouter'));
	Router::connect('/gestion-producteurs/editer/:slug-:id', array('controller' => 'Producteurs', 'action' => 'editer'), array('pass' => array('slug', 'id')));

/*Routes du controller Produits***/
	Router::connect('/', array('controller' => 'Entreprises', 'action' => 'accueil'));
	//Page de listing de tous les produits sans filtrage
	Router::connect('/nos-produits', array('controller' => 'Produits', 'action' => 'liste'));
	//Page affichant les produits selon la catégorie
	Router::connect('/nos-produits/:slug', array('controller' => 'Produits', 'action' => 'liste'), array('pass' => array('slug')));
	//Page de détails d'un produit
	Router::connect('/nos-produits/details/:slug-:id', array('controller' => 'Produits', 'action' => 'details'), array('pass' => array('slug', 'id')));
	Router::connect('/inventaire', array('controller' => 'Produits', 'action' => 'inventaire'));
	Router::connect('/inventaire/ajouter', array('controller' => 'Produits', 'action' => 'ajouter'));
	Router::connect('/inventaire/editer/:slug-:id', array('controller' => 'Produits', 'action' => 'editer'), array('pass' => array('slug', 'id')));

/*Routes du controller Rabais***/
	Router::connect('/gestion-rabais/:slug-:id', array('controller' => 'Rabais', 'action' => 'liste'), array('pass' => array('slug', 'id')));
	Router::connect('/gestion-rabais/supprimer', array('controller' => 'Rabais', 'action' => 'supprimer'));
	Router::connect('/gestion-rabais/ajouter/:slug-:id', array('controller' => 'Rabais', 'action' => 'ajouter'), array('pass' => array('slug', 'id')));
	Router::connect('/gestion-rabais/editer', array('controller' => 'Rabais', 'action' => 'editer'));

/*Routes du controller Regions***/
	Router::connect('/les-regions', array('controller' => 'Regions', 'action' => 'index'));
	Router::connect('/les-regions/:slug', array('controller' => 'Regions', 'action' => 'details'), array('pass' => array('slug')));
	Router::connect('/gestion-regions', array('controller' => 'Regions', 'action' => 'liste'));
	Router::connect('/gestion-region/:slug-:id', array('controller' => 'Regions', 'action' => 'gerer'), array('pass' => array('slug', 'id')));
	Router::connect('/gestion-regions/ajouter', array('controller' => 'Regions', 'action' => 'ajouter'));
	Router::connect('/gestion-regions/editer/:slug-:id', array('controller' => 'Regions', 'action' => 'editer'), array('pass' => array('slug', 'id')));

/* Routes du controller TypesSelections***/
	Router::connect('/nos-selections', array('controller' => 'TypesSelections', 'action' => 'index'));
	Router::connect('/gestion-selections', array('controller' => 'TypesSelections', 'action' => 'liste'));
	Router::connect('/gestion-selections/ajouter', array('controller' => 'TypesSelections', 'action' => 'ajouter'));
	Router::connect('/gestion-selections/supprimer/:id', array('controller' => 'TypesSelections', 'action' => 'supprimer'), array('pass' => array('id')));
	Router::connect('/gestion-selections/:slug-:id', array('controller' => 'TypesSelections', 'action' => 'gerer'), array('pass' => array('slug', 'id')));
	Router::connect('/gestion-selections/editer/:slug-:id', array('controller' => 'TypesSelections', 'action' => 'editer'), array('pass' => array('slug', 'id')));

/* Routes du controller Selections***/
	Router::connect('/gestion-packs/:slug-:id/ajouter-un-produit', array('controller' => 'Selections', 'action' => 'pack_ajout'), array('pass' => array('slug', 'id')));
	Router::connect('/gestion-packs/:slug/editer/:id', array('controller' => 'Selections', 'action' => 'pack_editer'), array('pass' => array('slug', 'id')));

/*Routes du controller Utilisateurs***/
	Router::connect('/connexion', array('controller' => 'Utilisateurs', 'action' => 'connexion'));
	Router::connect('/inscription', array('controller' => 'Utilisateurs', 'action' => 'inscription'));
	Router::connect('/deconnexion', array('controller' => 'Utilisateurs', 'action' => 'deconnexion'));
	Router::connect('/mon-compte', array('controller' => 'Utilisateurs', 'action' => 'compte'));
	Router::connect('/editer-mon-compte', array('controller' => 'Utilisateurs', 'action' => 'editer'));
	Router::connect('/configuration_totp', array('controller' => 'Utilisateurs', 'action' => 'totp_config'));
	Router::connect('/verification_totp', array('controller' => 'Utilisateurs', 'action' => 'verification_totp'));
	Router::connect('/gestion-utilisateurs', array('controller' => 'Utilisateurs', 'action' => 'listing'));

/* Routes du controller Transactions*/
	Router::connect('/transactions', array('controller' => 'Transactions', 'action' => 'listing'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
