<?php
App::uses('AppController', 'Controller');

class EntreprisesController extends AppController{
	//Définitions des méhodes accessibles depuis le controller lorsque l'on est hors connexion
	public function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow('accueil', 'liste', 'details');
        //tableau des views admin pour ce controller
        $views_admin = array('inventaire', 'ajouter', 'editer');
        //Vérification des droits de l'utilisateur selon la page
        if (!empty($this->Auth->user('id'))){
            //Test des droits de l'utilisateur
            if(in_array($this->action, $views_admin)){
                //On définit le layout au layout connected
                $this->layout = 'connected';
                if($this->Auth->user('id_type_utilisateur') != 3 && $this->Auth->user('id_type_utilisateur') != 4){
                    $this->Session->setFlash("Page inaccessible", 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
                    $this->redirect($this->referer());
                }
            }
        }
    }
    //Va contenir les options du stock
    public $stock_tbl = array('-' => '', 'Stock limité' => 'Stock limité', 'Rupture de stock' => 'Rupture de stock', 'Prochainement en ligne' => 'Prochainement en ligne');
    //On déclare utiliser la pagination
    public $components = array('Paginator');
    public $helpers = array('Paginator' => array('Paginator'), 'Text');

    //Page d'accueil
    public function accueil()
    {
        //On définit le titre de la page
        $this->set('title_for_layout', 'L\'annuaire des PME Suisses | SwissDirectories.PRO');
        $this->set('description', "SwissDirectories.PRO: votre annuaire et répertoire interactif d'entreprises situées en Suisse romande.
				Découvrez les offres proposées par les PME Suisses.");
        $d = array();
        $this->set($d);
    }
    //Cette fonction va afficher une liste de entreprises selon les paramètres soumis
    public function liste($slug = null)
    {
        //On définit le titre de la page
        $this->set('title_for_layout', 'Nos entreprises &middot; SferaVino.ch');
        //Récupération de toutes les régions
        $this->loadModel('Region');
        $this->Region->recursive = -1;
        $d['regions'] = $this->Region->find('all', array('conditions' => array('is_active' => 1, 'id_parent !=' => null)));
        //Récupération de toutes les catégories de entreprises
        $this->loadModel('Categorie');
        $this->Categorie->recursive = -1;
        $d['categories'] = $this->Categorie->findAllByIsActive(1);
        //Récupérationd des cépages
        $this->loadModel('Cepage');
        $this->Cepage->recursive = -1;
        $d['cepages'] = $this->Cepage->findAllByIsActive(1);
        //Récupérationd des producteurs
        $this->loadModel('Producteur');
        $this->Producteur->recursive = -1;
        $d['producteurs'] = $this->Producteur->findAllByIsActive(1);
        //création des conditions de base
        $conditions = array('entreprise.is_active ' => 1);
        //Si des data de recherche a été envoyé
        if(!empty($this->request->data)){
            if(!empty($this->request->data['entreprise']['term'])){
                //Récupération du term de recherche
                $term = $this->request->data['entreprise']['term'];
                //Ajout du term de recherche aux conditions de recherche
                $conditions['OR'] = array(array('entreprise.nom LIKE '=> "%".$term."%"), array('entreprise.description LIKE ' =>   "%".$term."%"));
            }else{
                //Récupération des données
                $d['entreprise'] = $this->request->data['entreprise'];
                //Filtre des différentes checkboxes
                $d['entreprise']['cepages'] = array_filter($d['entreprise']['cepages']);
                $d['entreprise']['categories'] = array_filter($d['entreprise']['categories']);
                $d['entreprise']['producteurs'] = array_filter($d['entreprise']['producteurs']);
                $d['entreprise']['regions'] = array_filter($d['entreprise']['regions']);

                //Ajout des conditions de catégorie
                if(count($d['entreprise']['categories']) == 0){
                    unset($d['entreprise']['categories']);
                }else{
                    $array = array();
                    //Si plus d'une catégorie a été sélectionné
                    if(count($d['entreprise']['categories']) > 1){
                        //ON parcours toutes les categories
                        foreach ($d['entreprise']['categories'] as $id => $value) {
                            $d['entreprise']['categories'][$id] = $id;
                            $array[]['entreprise.id_categorie'] = $id;
                        }
                        //Ajout des catégories aux conditions de recherche
                        $conditions['OR'] = $array;
                    }else{
                        foreach ($d['entreprise']['categories'] as $id => $value) {
                            $d['entreprise']['categories'][$id] = $id;
                            $conditions['entreprise.id_categorie'] = $id;
                        }
                    }
                }
                //Ajout des conditions de région
                if(count($d['entreprise']['regions']) == 0){
                    unset($d['entreprise']['regions']);
                }else{
                    $array = array();
                    //Si plus d'une region a été sélectionné
                    if(count($d['entreprise']['regions']) > 1){
                        //ON parcours toutes les regions
                        foreach ($d['entreprise']['regions'] as $id => $value) {
                            $d['entreprise']['regions'][$id] = $id;
                            $array[]['entreprise.id_region'] = $id;
                        }
                        //Ajout des régions aux conditions de recherche
                        $conditions['OR'] = $array;
                    }else{
                        foreach ($d['entreprise']['regions'] as $id => $value) {
                            $d['entreprise']['regions'][$id] = $id;
                            $conditions['entreprise.id_region'] = $id;
                        }
                    }
                }
                //Ajout des conditions de producteurs
                if(count($d['entreprise']['producteurs']) == 0){
                    unset($d['entreprise']['producteurs']);
                }else{
                    $array = array();
                    //Si plus d'une catégorie a été sélectionné
                    if(count($d['entreprise']['producteurs']) > 1){
                        //ON parcours toutes les producteurs
                        foreach ($d['entreprise']['producteurs'] as $id => $value) {
                            $d['entreprise']['producteurs'][$id] = $id;
                            $array[]['entreprise.id_producteur'] = $id;
                        }
                        //Ajout des producteurs aux conditions de recherche
                        $conditions['OR'] = $array;
                    }else{
                        foreach ($d['entreprise']['producteurs'] as $id => $value) {
                            $d['entreprise']['producteurs'][$id] = $id;
                            $conditions['entreprise.id_producteur'] = $id;
                        }
                    }
                }

                //Ajout de la condition pour le prix
                $conditions['entreprise.prix <='] = $d['entreprise']['max_prix'];
                $conditions['entreprise.prix >='] = $d['entreprise']['min_prix'];
            }
        }elseif(!empty($slug)){
            //Si un slug est envoyé en paramètre
            //On charge le model TypesSelection
            $this->loadModel('TypesSelection');
            $this->TypesSelection->recursive = -1;
            $producteur = $this->Producteur->findBySlug($slug);
            $categorie = $this->Categorie->findBySlug($slug);
            $region = $this->Region->findBySlug($slug);
            $cepage = $this->Cepage->findBySlug($slug);
            $type_selection = $this->TypesSelection->findBySlug($slug);
            //Récupération des différentes possiblités
            if(!empty($producteur)){
                //Si le slug correspond à un producteur
                $conditions['entreprise.id_producteur'] = $producteur['Producteur']['id'];
                //Récupértion de l'image du header
                $d['header_img'] = $producteur['Producteur']['img'];
                //récupération du nom du producteur
                $d['nom'] = $producteur['Producteur']['nom'];
                //On définit le titre de la page
                $this->set('title_for_layout', 'Découvrir les vins du producteur '.$producteur['Producteur']['nom'].' &middot; SferaVino.ch');
                $this->set('description', $producteur['Producteur']['seo_description']);
            }
            elseif(!empty($categorie)) {
                //Si le slug correspond à une catégorie
                $conditions['entreprise.id_categorie'] = $categorie['Categorie']['id'];
                //Récupértion de l'image du header
                $d['header_img'] = $categorie['Categorie']['img'];
                $d['nom'] = $categorie['Categorie']['nom'];
                //On définit le titre de la page
                $this->set('title_for_layout', 'Découvrir les '.$categorie['Categorie']['nom'].' de Suisse & Italie &middot; SferaVino.ch');
                $this->set('description', $categorie['Categorie']['seo_description']);
            }
            elseif(!empty($region)) {
                //Si le slug correspond à une région
                $conditions['entreprise.id_region'] = $region['Region']['id'];
                //Récupértion de l'image du header
                $d['header_img'] = $region['Region']['img'];
                $d['nom'] = $region['Region']['nom'];
                //On définit le titre de la page
                $this->set('title_for_layout', $region['Region']['seo_titre'].' &middot; SferaVino.ch');
                $this->set('description', $region['Region']['seo_description']);
            }
            elseif(!empty($cepage)){
                //Si le slug correspond à un cépage
                $this->loadModel('Cepagesentreprise');
                $this->Cepagesentreprise->recursive = -1;
                //liste des entreprises correspondant au cépages
                $entreprise_ids = $this->Cepagesentreprise->find('all', array('conditions' => array('Cepagesentreprise.id_cepage' => $cepage['Cepage']['id'])));
                //ON raccourci le tableau affin de garder uniquement une liste des id de entreprises qui utilise ce cépage
                $entreprise_ids = Set::Combine($entreprise_ids, '{n}.Cepagesentreprise.id_entreprise', '{n}.Cepagesentreprise.id_entreprise');
                $conditions['entreprise.id'] = $entreprise_ids;
                $d['nom'] = $cepage['Cepage']['nom'];
                //On définit le titre de la page
                $this->set('title_for_layout', $cepage['Cepage']['nom'].'| Découvrez les vins du cépage : '.$cepage['Cepage']['nom'].' &middot; SferaVino.ch');
                $this->set('description', 'Découvrez tous nos vins du cépage : '.ucwords($cepage['Cepage']['nom']));
            }elseif(!empty($type_selection)) {
                //Récupértion de l'image du header
                $d['header_img'] = $type_selection['TypesSelection']['img'];
                $d['nom'] = $type_selection['TypesSelection']['type'];
                //Si le slug correspond à un type de sélection
                $this->loadModel('Selection');
                $this->Selection->recursive = -1;
                //liste des entreprises correspondant à la sélection
                $entreprise_ids = $this->Selection->find('all', array('conditions' => array('Selection.id_types_selection' => $type_selection['TypesSelection']['id'])));
                //ON raccourci le tableau affin de garder uniquement une liste des id de entreprises qui utilise ce cépage
                $entreprise_ids = Set::Combine($entreprise_ids, '{n}.Selection.id_entreprise', '{n}.Selection.id_entreprise');
                $conditions['entreprise.id'] = $entreprise_ids;
                //On définit le titre de la page
                $this->set('title_for_layout', $type_selection['TypesSelection']['seo_titre'].'  &middot; SferaVino.ch');
                $this->set('description', $type_selection['TypesSelection']['seo_description']);
            }
        }
        //On définit la pagination des résultats
        $this->paginate = array(
            'conditions' => $conditions,
            'order' => array('entreprise.id' => 'DESC'),
            'limit' => 100
        );

        //récupération du titre de la page
        if(!empty($slug)){
            $slug = str_replace('-', ' ', $slug);
            $d['slug'] = $slug;
        }
        //On exécute la recherche
        $d['entreprises'] = $this->paginate('entreprise');

        if(!empty($d['entreprise']['cepages']) && !empty($d['entreprises'])){
            //ON filtre si des cepages ont été envoyé
            $d['entreprise']['cepages'] = array_keys($d['entreprise']['cepages']);
            $d['entreprises'] = $this->Entreprise->filtre_cepage($d['entreprises'], $d['entreprise']['cepages']);
        }
        //On envoi le résultat à la view
        $this->set($d);
    }

    //Cette fonction va afficher toutes les offres spéciales du site
    public function offres()
    {
        $this->Entreprise->contain(array('rabais'));
        $this->loadModel('Pack');
        $this->Pack->recursive = -1;
        //récupération des packs de entreprise
        $d['packs'] = $this->Pack->find('all', array('conditions' => array('Pack.is_active' => 1), 'order' => array('Pack.prix' => 'DESC')));
        //récupération des entreprises
        $d['entreprises'] = $this->Entreprise->find('all', array('conditions' => array('entreprise.is_active' => 1), 'order' => array('entreprise.prix' => 'DESC')));
        //On définit le titre de la page
        $this->set('title_for_layout', 'Nos offres - Vins à prix réduit - Vins Suisse & Italie &middot; SferaVino.ch');
        $this->set($d);
    }
    //Cette fonction va afficher la fiche d'information d'un entreprise
    public function details($slug = null, $id = null)
    {
        //Si le entreprise est introuvable
        if(!$id || !$this->Entreprise->findByIdAndIsActive($id, 1) && ($this->Auth->user('id_type_utilisateur') != 3 && $this->Auth->user('id_type_utilisateur') != 4)){
            //Message d'erreur et redirection vers la liste des entreprises
            $this->Session->setFlash('entreprise introuvable', 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
            $this->redirect(array('controller' => 'entreprises', 'action' => 'liste'));
        }else{
            $this->Entreprise->contain(array(
                'producteur' => array('fields' => array('id', 'nom', 'slug')),
                'categorie_entreprise' => array('fields' => array('id', 'nom', 'slug')),
                'region' => array('fields' => array('id', 'id_parent', 'nom', 'slug')),
                'recompenses' => array('fields' => array('titre')),
                'entreprise_cepage',
                'rabais'
            ));
            //Sinon on récupère le entreprises ainsi que tous les avis qui lui sont attributés
            $d['entreprise'] = $this->Entreprise->findById($id);

            //Récupération du pays
            $this->loadModel('Region');
            $this->Region->recursive = -1;
            $d['pays'] = $this->Region->findById($d['entreprise']['region']['id_parent']);
            //Corrections du texte pour le stock
            if(!in_array($d['entreprise']['entreprise']['stock'], $this->stock_tbl)){
                $d['entreprise']['entreprise']['stock'] = "";
            }
            //Envoi des données à la view
            //On définit le titre de la page
            $this->set('title_for_layout', 'Découvrir le '.$d['entreprise']['entreprise']['nom'].' - '.ucwords($d['entreprise']['categorie_entreprise']['nom']).' &middot; SferaVino.ch');
            $this->set('description', $d['entreprise']['entreprise']['seo_description']);
            //Envoi des données à la view
            $this->set($d);
        }
    }
    //Cette fonction va afficher l'inventaire des entreprises aux manager et administrateur
    public function inventaire()
    {
        $this->Entreprise->contain(array('producteur' => array('fields' => array('nom'))));
        //On définit le titre de la page
        $this->set('title_for_layout', 'Gestion des entreprises &middot; SferaVino.ch');
        //Récupération de tous les entreprises
        $d['entreprises'] = $this->Entreprise->find('all');
        $d['stock_tbl'] = $this->stock_tbl;
        $this->set($d);
    }
    //Cette fonction va permettre à l'administrateur de créer un nouveau entreprise
    public function ajouter()
    {
        //Récupérationd des cépages
        $this->loadModel('Cepage');
        $this->loadModel('Categorie');
        $this->loadModel('Producteur');
        $this->loadModel('Region');
        if($this->request->data){
            //Récupération des donneés
            $d = $this->request->data;
            foreach ($d['entreprise'] as $key => $value) {
                if (substr($key, 0, 6) === 'recomp') {
                    $d['entreprise']['recompenses'][] = $value;
                }
            }
            $img = $d['entreprise']['img'];
            if(empty($d['entreprise']['img']['tmp_name'])){
                unset($d['entreprise']['img']);
            }
            //On créer l'entité à sauvegarder
            $this->Entreprise->create($d);
            //On valide et enregistre les données
            if($this->Entreprise->save()){
                //Enregistrement du chemin de l'image dans les data du nouveau entreprise
                if(!empty($img['name'])){
                    //Enregistrement de l'image du entreprise dans la ligne du entreprise
                    $this->Entreprise->id = $this->Entreprise->getInsertID();
                    //Récupération de l'extension de l'image
                    $info = pathinfo($img['name']);
                    $this->Entreprise->saveField('img', 'entreprises/'.$this->Entreprise->id.'.'.$info['extension'], array('callbacks' => false));
                }
                //Affichage du message de succès et redirection vers l'inventaire
                $this->Session->setFlash('entreprise ajouté !', 'flash_notif', array('type' => 'success', 'titre' => 'Succès'));
                $this->redirect(array('controller' => 'entreprises', 'action' => 'inventaire'));
            }else{
                //Affichage d'une erreur flash
                $this->Session->setFlash('Veuillez vérifier le formulaire', 'flash_notif', array('type' => 'error', 'titre' => 'Erreur de validation du formulaire'));
            }
        }
        $d['cepages'] = $this->Cepage->find('list', array('fields' => array('id', 'nom')));
        $d['categories'] = $this->Categorie->find('list', array('fields' => array('id', 'nom'), 'conditions' => array('is_active' => 1)));
        $d['producteurs'] = $this->Producteur->find('list', array('fields' => array('id', 'nom')));
        $d['regions'] = $this->Region->find('list', array('fields' => array('id', 'nom'), 'conditions' => array('is_active' => 1, 'id_parent !=' => null)));
        //On définit le titre de la page
        $this->set('title_for_layout', 'Ajouter un entreprise à l\'inventaire &middot; SferaVino.ch');
        $this->set($d);
    }
    //Cette fonction va permettre à l'administrateur d'éditer un entreprise
    public function editer($slug = null, $id = null)
    {
        //Si le formulaire est envoyé
        if($this->request->data){
            //Récupération des donneés
            $d = $this->request->data;
            $img = $d['entreprise']['img'];
            if(empty($d['entreprise']['img']['tmp_name'])){
                unset($d['entreprise']['img']);
            }
            //On valide et enregistre les données
            if($this->Entreprise->save($d)){
                //Enregistrement du chemin de l'image dans les data du nouveau entreprise
                if(!empty($img['name'])){
                    //Récupération de l'extension de l'image
                    $info = pathinfo($img['name']);
                    $this->Entreprise->saveField('img', 'entreprises/'.$id.'.'.$info['extension'], array('callbacks' => false));
                }
                //Affichage du message de succès et redirection vers l'inventaire
                $this->Session->setFlash('entreprise éditer !', 'flash_notif', array('type' => 'success', 'titre' => 'Succès'));
                $this->redirect(array('controller' => 'entreprises', 'action' => 'inventaire'));
            }else{
                //Affichage d'une erreur flash
                $this->Session->setFlash('Veuillez vérifier le formulaire', 'flash_notif', array('type' => 'error', 'titre' => 'Erreur de validation du formulaire'));
            }
        }
        $id = intval($id);
        //Si le entreprise est introuvable
        if(!$id || !$this->Entreprise->findById($id)){
            //Message d'erreur et redirection vers la liste des entreprises
            $this->Session->setFlash('entreprise introuvable', 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
            $this->redirect($this->referer());
        }
        //Récupérationd des cépages
        $this->loadModel('Cepage');
        $this->loadModel('Categorie');
        $this->loadModel('Producteur');
        $this->loadModel('Region');
        //Récupération des data nécessaire  la page d'édition
        $d['cepages'] = $this->Cepage->find('list', array('fields' => array('id', 'nom')));
        $d['categories'] = $this->Categorie->find('list', array('fields' => array('id', 'nom'), 'conditions' => array('is_active' => 1)));
        $d['producteurs'] = $this->Producteur->find('list', array('fields' => array('id', 'nom')));
        $d['regions'] = $this->Region->find('list', array('fields' => array('id', 'nom'), 'conditions' => array('is_active' => 1, 'id_parent !=' => null)));
        $d['stock'] = $this->stock_tbl;
        //On définit le titre de la page
        $this->set('title_for_layout', 'Ajouter un entreprise à l\'inventaire &middot; SferaVino.ch');
        $this->set($d);
        //Sinon on récupère le entreprises ainsi que tous les avis qui lui sont attributés
        $d['entreprise'] = $this->Entreprise->findById($id);
        //On définit le titre de la page
        $this->set('title_for_layout', 'Sfera Vino | Édition du vin : '.$d['entreprise']['entreprise']['nom']);
        //Envoi des données à la view
        $this->set($d);
    }
    //Va permettre d'ajouter un entreprise au panier
    public function add_cart()
    {
        //Cette fonction n'est pas visible
        $this->autoRender = false;
        //Uniquement accès ajax
        $this->request->onlyAllow('ajax');
        //Déclaration de la variable panier
        $panier = array();
        $model = 'entreprise';
        $this->loadModel('Pack');
        if($this->request->data['type_entreprise'] == 'pack'){
            $model = 'Pack';
            //Mettre le modèle en récursive -1
            $this->$model->contain(array('entreprises_pack'));
        }
        //Test si un id de entreprise à été envoyer pour l'ajout au panier
        if(!empty($this->request->data['id_entreprise']) && $this->request->data['id_entreprise'] > 0 && $this->$model->findByIdAndIsActive($this->request->data['id_entreprise'], 1)){
            //Création du contenu du message de succès
            $alert = array('message' => $model." ajouté à votre panier", 'title' => 'Succès', 'type' => 1);
            //Récupération de l'id du entreprise
            $id_entreprise = $this->request->data['id_entreprise'];
            //Récupération des rabais associé uniquement
            $this->Entreprise->contain(array('rabais'));
            //Récupération des informations du entreprise
            $entreprise = $this->$model->findById($this->request->data['id_entreprise']);
            //Par défaut, la quantité d'ajout d'un entreprise est 1
            $quantite = 1;
            //Test si la quantité a été envoyer avec le entreprise
            if(!empty($this->request->data['quantite']) && $this->request->data['quantite'] > 0){
                //On récupère la quantité dans notre variable
                $quantite = $this->request->data['quantite'];
            }
            //index
            $cle = 1;
            //Va nous permettre de savoir si le entreprise est déjà dans le panier
            $entreprise_existe = 0;
            //Si le panier existe déjà et que l'article est déjà dans le panier
            if($this->Session->check('panier')){
                //Récupération du panier
                $panier = $this->Session->read('panier');
                unset($panier['frais_livraison']);
                //On va checker si le entreprise n'est pas déjà quelque part dans le panier
                foreach ($panier as $key => $item) {
                    //Si l'item du panier contient une colonne du même nom que le model actuel, il s'agit d'un entreprise du même type
                    if(array_key_exists($model, $item)){
                        //Donc on peut comparer s'il s'agit du même entreprise
                        if($item[$model]['id'] == $id_entreprise){
                            $quantite = intval($item['quantite']) + $quantite;
                            $cle = $key;
                            $entreprise_existe = 1;
                            break;
                        }
                    }
                }
                //Si le flag entreprise existe est encore a zéro, on créer une nouvelle ligne dans le panier
                if($entreprise_existe == 0){
                    end($panier);
                    $cle = key($panier);
                    $cle++;
                    reset($panier);
                }
            }
            //Ajout du entreprise au panier
            $panier[$cle] = $entreprise;
            //Ajout de la quantité du entreprise dans les data du panier
            $panier[$cle]['quantite'] = $quantite;
            $this->Session->write('panier', $panier);
            $total_entreprises = count($panier); //count total items
            return json_encode(array('nbr_entreprises'=>$total_entreprises, 'alert' => $alert, ));
        }else{
            //Création du contenu du message de succès
            $alert = array('message' => $model." indsponible", 'title' => 'Action impossible', 'type' => 2);
            //On renvoi l'erreur
            return json_encode(array('alert' => $alert, ));
        }
    }
    //Va permettre de supprimer un élément du panier
    public function delete_entreprise_cart()
    {
        // Cette fonction n'est pas visible
        $this->autoRender = false;
        $panier = null;
        $panier = $this->Session->read('panier');
        // NPas d'accès direct, uniquement accès ajax
        $this->request->onlyAllow('ajax');
        if(!empty($this->request->data['id_entreprise']) && $this->request->data['id_entreprise'] > 0 && !empty($panier)){
            $alert = array('message' => "entreprise supprimer de votre panier", 'title' => 'Succès', 'type' => 1);
            //Récupération de l'id du entreprise selon l'indexation du panier
            $id_entreprise   = $this->request->data['id_entreprise'];
            //Récupération du panier
            $panier = $this->Session->read('panier');
            //Si le entrepriseest bien dans le panier, on le supprime
            if(!empty($panier[$id_entreprise]))
            {
                $model = 'entreprise';
                if(array_key_exists('Pack', $panier[$id_entreprise])){
                    $model = 'Pack';
                }
                $total =  $this->Session->read('total_panier');
                $total = $total - intval($panier[$id_entreprise][$model]['prix']);
                $this->Session->write('total_panier', $total);
                unset($panier[$id_entreprise]);
            }else{
                $alert = array('message' => "Ce entreprise n'existe pas, veuillez réessayer", 'title' => 'Infos', 'type' => 2);
            }
            $frais = 0;
            //Suppression des frais de livraison du panier
            if(array_key_exists('frais_livraison', $panier)){
                $frais = $panier['frais_livraison'];
                unset($panier['frais_livraison']);
            }
            //On recalcul le nombr ed'item dans le panier
            $total_entreprises = count($panier);
            //On recharge la session avec le entreprise en moins
            $frais =  number_format($frais, 2, '.', '');
            $panier['frais_livraison'] = $frais;
            $this->Session->write('panier', $panier);
            return json_encode(array('nbr_entreprises'=>$total_entreprises, 'alert' => $alert));
        }
    }
    //Va permettre de mettre à jour le panier html
    public function update_cart()
    {
        // Cette fonction n'est pas visible
        $this->autoRender = false;
        // NPas d'accès direct, uniquement accès ajax
        $this->request->onlyAllow('ajax');

        //ON initialise la variable qui va contenir le total
        $total = 0;
        //Si l'argument update est envoyé avec la bonne valeur, on met à jour le panier
        if(!empty($this->request->data['update']) && $this->request->data['update'] == 1){
            //Si le panier n'est pas vide, on va mettre à jour l'HTML
            $panier = null;
            $panier = $this->Session->read('panier');
            if($this->Session->check('panier') && !empty($panier)){
                //Récupération du panier
                $panier = $this->Session->read('panier');
                unset($panier['frais_livraison']);
                $html_panier = '<ul class="whishlist">';
                //On va ajouter le html de chaque entreprise dans le preview correspondant
                foreach ($panier as $key => $entreprise) {
                    //le model à utilisé est par défaut entreprise
                    $model = 'entreprise';
                    if(array_key_exists('Pack', $entreprise)){
                        $model = 'Pack';
                    }
                    if(!empty($entreprise['rabais'])){
                        foreach ($entreprise['rabais'] as $cle => $rabais) {
                            $today = strtotime(date('Y-m-d'));
                            $date_debut = strtotime($rabais['date_debut']);
                            $date_fin= strtotime($rabais['date_fin']);
                            $panier[$key]['prix_avec_rabais'] = $entreprise[$model]['prix'];
                            if($rabais['is_active'] == 1 && $today >= $date_debut && $today <= $date_fin){
                                $entreprise[$model]['prix'] = $entreprise[$model]['prix'] - ($entreprise[$model]['prix'] / 100 * $rabais['pourcentage_rabais']);
                                $panier[$key]['prix_avec_rabais'] = $entreprise[$model]['prix'];
                                break;
                            }
                        }
                    }
                    //Récupération du sous total
                    $sous_total = floatval($entreprise[$model]['prix'] * $entreprise['quantite']);
                    $sous_total = round($sous_total, 1);
                    //Affichage du prix au format 000.00
                    if(is_float($sous_total)){
                        $sous_total =  number_format($sous_total, 2, '.', '');
                    }
                    //Ajout de l'article dans le html du panier
                    $html_panier .=  '
                    <li>
                        <div class="whishlist-item">
                            <div class="product-image">
                                <a href="'.Router::url('/', true).'nos-entreprises/details/'.$entreprise[$model]['slug'].'-'.$entreprise[$model]['id'].'" title=" Voir les détails du entreprise : '.$entreprise[$model]['nom'].'">
                                    <img src="'.Router::url('/', true).'img/'.$entreprise[$model]['img'].'" alt="image du entreprise :'.$entreprise[$model]['nom'].'">
                                </a>
                            </div>
                            <div class="product-body">
                                <div class="whishlist-name">
                                    <h3>
                                        <a href="'.Router::url('/', true).'nos-entreprises/details/'.$entreprise[$model]['slug'].'-'.$entreprise[$model]['id'].'" title=" Voir les détails du entreprise : '.$entreprise[$model]['nom'].'">'.$entreprise[$model]['nom'].'</a>
                                    </h3>
                                </div>
                                <div class="whishlist-price">
                                    <span>Sous-total : </span><strong>'.$sous_total.' CHF</strong>
                                </div>
                                <div class="whishlist-quantity">
                                    <span>Quantité : </span><span>'.$entreprise['quantite'].'</span>
                                </div>
                            </div>
                        </div>
                        <a href="#" title="" class="remove delete_panier" data-id="'.$key.'"><i class="icon icon-remove"></i></a>
                    </li>';
                    //$sous_total = ($entreprise_price * $entreprise_qty);
                    $total = ($total + $sous_total);
                }
                $total = round($total, 1);
                //Affichage du prix au format 000.00
                $total =  number_format($total, 2, '.', '');

                //Après avoir ajouter chaque article, on va maintenant afficher le total ainsi que les boutons de paiement et de détails du panier
                $html_panier .= '
                </ul>
                <div class="menu-cart-total">
                    <span>Total</span><span class="price">'.$total.' CHF</span>
                </div>
                <div class="cart-action">
                    <a href="'.Router::url('/', true).'commander" title="" class="btn btn-lg btn-primary btn-block">Commander</a>
                </div>';
                //On met à jour la variable de session $total_panier
                $this->Session->write('total_panier', $total);
                //On met à jour la variable de session du panier
                $this->Session->write('panier', $panier);
                return $html_panier; //exit and output content
            }else{
                $this->Session->write('total_panier', 0);
                //Renvoi false si le panier est vide
                return 'Panier vide';
            }
        }elseif(!empty($this->request->data['update']) && $this->request->data['update'] == 2){
            //Récupération du panier
            $panier = $this->Session->read('panier');
            $coordonnee = $this->Session->read('coordonnee');
            //récupération de la valeur total du panier
            $total = $this->Session->read('total_panier');
            $session_total = $this->Session->read('total_panier');
            //Si un pourcentage rabais est définit, alors on va mettre à jour le prix selon ce rabais
            if(!empty($coordonnee['Commande']['pourcentage_rabais'])){
                //Récupération du total sans rabais
                $coordonnee['Commande']['prix_sans_rabais'] = $total;
                //Calcul du total avec rabais
                $total = $total - (($total/100)*$coordonnee['Commande']['pourcentage_rabais']);
                $coordonnee['Commande']['prix_avec_rabais'] = $total;
                $coordonnee['Commande']['prix_paye'] = $coordonnee['Commande']['prix_avec_rabais'];
                $this->Session->write('coordonnee', $coordonnee);
            }
            //Recalcul des frais de livraison
            $this->loadModel('Commande');
            $total_panier = $total;
            $tbl_frais = Configure::read('frais_livraison');
            //Si le panier à au maximum 5 bouteille
            if($this->Commande->verif_nbr_btl($panier) <= 5){
                $panier['frais_livraison'] = $tbl_frais[0];
            }elseif ($this->Commande->verif_nbr_btl($panier) >= 6){
                $panier['frais_livraison'] = $tbl_frais[1];
            }
            //Si la valeur du panier est au dessus de 150chf
            if($total_panier >=150){
                $panier['frais_livraison'] = $tbl_frais[2];
            }
            /*if($total >= 300){
                $panier['frais_livraison'] = 0;
            }*/
            //On update le panier en session
            $panier['frais_livraison'] =  number_format($panier['frais_livraison'], 2, '.', '');
            $this->Session->write('panier', $panier);
            $total = $total + $panier['frais_livraison'];
            //if(is_float($total)){
            $total =  number_format($total, 2, '.', '');
            //}
            $this->Session->write('total_panier_avec_livraison', $total);
            $html = '
            <table>
                <tbody>
                    <tr class="cart-subtotal">
                        <th>Sous-total (TVA incluse) :</th>
                        <td><span class="amount">'.$session_total.' CHF</span>
                        </td>
                    </tr>
                    <tr class="shipping">
                        <th>Frais de livraison:</th>
                        <td>'.$panier['frais_livraison'].' CHF</td>
                    </tr>';
                    //Si un rabais a été créer, on va l'afficher
                    if($total < $session_total){
                        $rabais = $session_total-$total;

                        $html .='
                        <tr class="shipping">
                            <th>Code Promo:</th>
                            <td>- '.$rabais.' CHF</td>
                        </tr>';
                    }
                    $html .='
                    <tr class="order-total">
                        <th>Total (TVA incluse):</th>
                        <td><strong><span class="amount">'.$total.' CHF</span></strong>
                        </td>
                    </tr>
                </tbody>
            </table>';
            return $html;
        }
    }
}
?>
