<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
 class AppController extends Controller {
     //On inclut le helper html et session
     public $helpers = array('Html', 'Session');
     //Définitions des composants que l'on va utiliser dans notre site
     public $components = array(
       'DebugKit.Toolbar',
       'Session',
       'Cookie',
       'Auth' => array(
           'authenticate' => array(
               'Form' => array(
                   'scope' => array('Utilisateur.active' => 1, 'Utilisateur.conditions_generales' => 1),
                   'userModel' => 'Utilisateur',
                   'fields' => array(
                       'username' => 'email'
                   )
               )
           )
       )
     );

     function beforeFilter() {
         $this->Cookie->httpOnly = true;
         if (!$this->Auth->loggedIn() && $this->Cookie->read('rememberMe')){
                 $cookie = $this->Cookie->read('rememberMe');
                 $this->loadModel('Utilisateur'); // If the User model is not loaded already
                 $user = $this->Utilisateur->find('first',
                     array(
                         'conditions' => array(
                                 'Utilisateur.email' => $cookie['email'],
                                 'Utilisateur.mot_de_passe' => $cookie['password']
                         )
                     )
                 );
             if($user && !$this->Auth->login($user['Utilisateur'])){
                 $this->redirect('/Utilisateurs/deconnexion');
             }
             else{
                 $this->redirect($this->Auth->redirectUrl());
                 // redirect to Auth.redirect if it is set, else to Auth.loginRedirect ('/users/userhome') if it is set, else to /
             }
         }
         //On définit la page de connexion par défaut
         $this->Auth->loginAction = array('controller'=>'Utilisateurs', 'action'=>'connexion');
         //Lors de son chargement, le composent Auth bloque l'accès à toutes les pages du site
         $this->Auth->deny();
     }
 }
