<?php
App::uses('AppController', 'Controller');


class UtilisateursController extends AppController{

  public function beforeFilter(){
      parent::beforeFilter();
      //Définitions des méhodes accessibles depuis le controller lorsque l'on est hors connexion
      $this->Auth->allow('inscription','activate', 'connexion', 'verification_totp', 'forgot', 'mot_de_passe', 'show', 'facebook_connect');
      //tableau des views admin pour ce controller
      $views_admin = array('listing', 'desactiver', 'activer');
      //Vérification des droits de l'utilisateur selon la page
      if (!empty($this->Auth->user('id'))){
          //Test des droits de l'utilisateur
          if(in_array($this->action, $views_admin)){
              //On définit le layout au layout connected
              $this->layout = 'connected';
              if($this->Auth->user('id_type_utilisateur') != 3 && $this->Auth->user('id_type_utilisateur') != 4){
                  $this->Session->setFlash("Page inaccessible", 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
                  $this->redirect($this->referer());
              }
          }
      }
  }

  //On déclare utiliser la pagination
  public $components = array('Paginator');
  public $helpers = array('Paginator' => array('Paginator'));

  //Cette méthode va s'occuper de l'inscrption de l'utilisateur
  public function inscription(){
      //Définition de la balise title
      $this->set('title_for_layout', 'SwissDirectories.PRO | Créer un compte et enregistrer votre entreprise dans l\'annuaire des PME Suisses');
      //Si les données envoyé par le formulaire ne sont pas vide
      if (!empty($this->request->data)) {
          //Récupération des donées dans une varaible $d
          $d = $this->request->data;
          //die(debug($d));
          //On récupère la langue du browser de l'utilisateur
          $langue = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
          //Récupération de l'adresse ip de l'utilisateur
          $ip = $this->request->clientIp();
          //On charge le model avec les données reçu
          $this->Utilisateur->create($this->request->data);
          //On fait valider les données par le model
          if($this->Utilisateur->validates()){
              $token = md5(time() . ' - ' . uniqid());
              $user = array(
                  //ON hash le mot de pass avec la méthode password du composant Auth
                  'mot_de_passe' => $d['Utilisateur']['mot_de_passe'],
                  'email'     => $d['Utilisateur']['email'],
                  'token'    => $token,
                  'nom' => $d['Utilisateur']['nom'],
                  'prenom' => $d['Utilisateur']['prenom'],
                  'langue' => $langue,
                  'date_inscription' => date('Y-m-d'),
                  'adresse_ip' => $ip,
                  'conditions_generales' => 1,
                  'active' => 0
              );
              //On enregistre le nouvelle utilisateur dans la db
              if($this->Utilisateur->save($user, array('validates' => false))){
                  //Récupération de l'id de l'utilisateur inscrit
                  $user['id'] = $this->Utilisateur->getInsertID();
                  //Création de l'email
                  $user['template'] = "signup";
                  $user['sujet'] = "Bienvenue sur SwissDirectories.PRO !";
                  $user['destinataire'] = $user['email'];
                  //On envoi un mail de validation du compte au nouvel utilsateur
                  /*$this->loadModel('Contact');
                  if($this->Contact->send_mail($user, 1)){
                    Debug('email envoyé');
                  }else{
                    debug("erreur d'envoi");
                  }
                  die();*/
                  //affichage du message de succès de l'inscription
                  $this->Session->setFlash('Votre compte SwissDirectories.PRO a été crée avec succès', 'flash_notif', array('type' => 'success', 'titre' => 'Inscription réussi !'));
              }else{
                  //affichage du message de succès de l'inscription
                  $this->Session->setFlash('Une erreur inattendu s\'est produite. Veuillez réessayer', 'flash_notif', array('type' => 'error', 'titre' => 'Erreur lors de l\'inscription'));
              }
              $this->redirect("/");
          }else{
              //Si les donneés ne sont pas valide, on affiche un message d'erreur
              $this->Session->setFlash('Veuillez corriger les erreurs', 'flash_notif', array('type' => 'error', 'titre' => 'Formulaire invalide ou incomplet'));
          }
      }
  }

  //Cette méthode va activer le compte de l'utilisateur
  //Elle prend en paramètre l'id de l'utilisateur ainsi que le token fournit par l'email d'activation
  public function activate($user_id, $token){
      $this->Utilisateur->recursive = -1;
      $user = $this->Utilisateur->find('first', array(
          'conditions' => array('Utilisateur.id' => $user_id, 'token' => $token, 'is_active' => 0)
      ));
      //Si l'utilisateur n'existe pas, alors on redirige vers la page d'accueil
      if (empty($user)) {
          //Message d'erreur avant redirection vers l'accueil
          $this->Session->setFlash('Ce lien de validation est inutilisable', 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
          return $this->redirect('/');
      }else{
          //On met à jour les champs nécessaire à l'activation du compte de l'utilisateur
          $this->Utilisateur->id = $user['Utilisateur']['id'];
          $this->Utilisateur->saveField('token', '', array('callbacks' => false));
          $this->Utilisateur->saveField('is_active', 1, array('callbacks' => false));

          //Si l'utilisateur existe bien, on va chercher toutes les commandes qui devraient lui être affiliés
          //On chargele model commande
          $this->loadModel('Commande');
          $this->Commande->find_commandes($user['Utilisateur']['id'], $user['Utilisateur']['email']);

          //Message flash pour indquer que le compte est maintenant actif
          $this->Session->setFlash('Votre compte est maintenant actif, connectez-vous !', 'flash_notif', array('type' => 'success', 'titre' => 'Succès'));
          $this->redirect(array('action' => 'connexion'));
      }
  }

  //Cette méthode va s'occuper de la connexion de l'utilisateur
  public function connexion(){
      $this->Utilisateur->recursive = -1;
      //ON définit le titre de la page
      $this->set('title_for_layout', 'Connexion à votre espace utilisateur | SwissDirectories.PRO');
      //Si les données envoyé ne sont pas vide
      if($this->request->is('post') && !$this->Auth->user('id')) {
          //Récupération des données dans le tableau $d
          $d = $this->request->data;
          $this->Session->write('user_connexion', $d);
          $d['Utilisateur']['mot_de_passe'] = Security::hash($this->data['Utilisateur']['mot_de_passe'], null, true);
          $user = $this->Utilisateur->find('first', array(
            'email' => $d['Utilisateur']['email'],
            'mot_de_passe' => $d['Utilisateur']['mot_de_passe']/*,
            'active' => 1*/
          ));
          //Si l'utilisateur a bien un compte
          if($user){
            //On effectue la connexion via le composant auth
            if ($this->Auth->login()) {
                //Si l'utilisateur est connecté au site et qu'il veut qu'on se souvienne de lui
                if (isset($this->request->data['Utilisateur']['rememberMe']) && $this->request->data['Utilisateur']['rememberMe'] == 1) {
                    // Le temps d'expiration du cookie est de 2 semaine
                    $cookieTime = "2 weeks";
                    // remove "remember me checkbox"
                    unset($this->request->data['Utilisateur']['rememberMe']);
                    // on hash le mot de passe de l'utilisateur
                    $this->request->data['Utilisateur']['mot_de_passe'] = Security::hash($this->request->data['Utilisateur']['mot_de_passe'], null, true);
                    //écriture du cookie
                    $this->Cookie->write('rememberMe', $this->request->data['Utilisateur'], true, $cookieTime);
                }
                //ON met à jour la date de dernière connexion de l'utilisateur
                $this->Utilisateur->id = $this->Auth->User('id');
                $this->Utilisateur->saveField('date_connexion', date('Y-m-d H:i:s'), array('callbacks' => false));
                //Message de succès de la connexion
                $this->Session->setFlash("Vous êtes maintenant connecté", 'flash_notif', array('type' => 'success', 'titre' => 'Succès'));
                //Redirection après connexion
                if($this->Auth->user('id_type_utilisateur') == 3 || $this->Auth->user('id_type_utilisateur') == 4){
                    //Redirection vers la page de gestiond es commandes
                    $this->redirect(array('controller' => 'Commandes', 'action' => 'listing'));
                }else{
                    //Redirection vers la page de mes commandes
                    $this->redirect(array('controller' => 'Commandes', 'action' => 'mes_commandes'));
                }
            }else{
                $this->Session->setFlash('Identifiants incorrects', 'flash_notif', array('type' => 'error', 'titre' => 'Impossible de se connecter'));
            }
          }else{
              $this->Session->setFlash('Identifiants incorrects', 'flash_notif', array('type' => 'error', 'titre' => 'Impossible de se connecter'));
          }
      }elseif($this->Auth->user('id')){
         $this->redirect(array('controller' => 'Utilisateurs', 'action' => 'compte'));
      }
  }

  //Cette méthode va déconnecter l'utilisateur
  public function deconnexion(){
      // Cette fonction n'est pas visible
      $this->autoRender = false;
      $this->Auth->logout();
      if($this->Cookie->check('rememberMe')){
          //Si le cookie existe, on le supprimer
          $this->Cookie->delete('rememberMe');
      }
      $this->Session->setFlash("Vous êtes maintenant déconnecté", 'flash_notif', array('type' => 'success', 'titre' => 'Déconnecté'));
      return $this->redirect('/');
  }

  //Système de récupération de mot de passe oublié
  public function forgot(){
      $this->Utilisateur->recursive = -1;
      //ON définit le titre de la page
      $this->set('title_for_layout', 'SwissDirectories.PRO | Récupération de mot de passe');
      if (!empty($this->request->data)) {
          $user = $this->Utilisateur->findByEmail($this->request->data['Utilisateur']['email'], array('id'));
          if(empty($user)){
              //Création de la variable de session qui va contenir les données du formulaire
              $this->Session->write('data', $this->request->data);
              $this->Session->write('data.view', 3);
              //Création de la variable de session qui va contenir les erreurs de validation
              $this->Session->write('validation_errors.Utilisateur', $this->Utilisateur->validationErrors);
              //Si aucun utilisateur ne correspond à l'adresse mail, on affiche un message d'erreur
              $this->Session->setFlash("Cette adresse email n'est associé a aucun compte", 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
          }else{
              $token = md5(uniqid().time());
              $this->Utilisateur->id = $user['Utilisateur']['id'];
              $this->Utilisateur->saveField('token', $token, array('callbacks' => false));

              App::uses('CakeEmail', 'Network/Email');
              $mail = 'default';
              if($_SERVER['SERVER_NAME'] == 'localhost'){

                  $mail = 'gmail';
              }
              $CakeEmail = new CakeEmail($mail);
              $CakeEmail->to($this->request->data['Utilisateur']['email']);
              $CakeEmail->subject('Régénération de mot de passe');
              $CakeEmail->template('mot_de_passe');
              $CakeEmail->viewVars(array('token' => $token, 'id' => $user['Utilisateur']['id']));
              $CakeEmail->emailFormat('html');
              $CakeEmail->send();
              $this->Session->setFlash("Un email vous a été envoyé avec les instructions pour regénérer votre mot de passe", 'flash_notif', array('type' => 'alert-info'));
              $this->redirect('/');
          }
      }
  }

  //Méthode de modification du mot de passe oublié
  public function password($user_id, $token){
      $this->Utilisateur->recursive = -1;
      //ON définit le titre de la page
      $this->set('title_for_layout', 'SwissDirectories.PRO | Modifier le mot de passe');
      $user = $this->Utilisateur->find('first', array(
          'fields'     => array('id'),
          'conditions' => array('Utilisateur.id' => $user_id, 'Utilisateur.token' => $token)
      ));
      if (empty($user)) {
          $this->Session->setFlash('Ce lien ne semble pas bon', 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
          return $this->redirect(array('action' => 'forgot'));
      }
      //Si des données sont envoyé à
      if(!empty($this->request->data)){
          $this->Utilisateur->create($this->request->data);
          if($this->Utilisateur->validates()){
              //$this->Utilisateur->create();
              $this->Utilisateur->save(array(
                  'id' => $user['Utilisateur']['id'],
                  'token' => '',
                  'is_active' => 1,
                  'mot_de_passe' => $this->request->data['Utilisateur']['mot_de_passe']
              ));
              $this->Session->setFlash("Votre mot de passe a été réinitialisé avec succès", 'flash_notif', array('type' => 'success', 'titre' => 'Succès'));
              $this->redirect(array('action' => 'connexion'));
          }else{
              $this->Session->setFlash('Veuillez corriger les erreurs', 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
          }
      }
  }

  //Va afficher les données de l'utilisteur, et lui permettre de les modifier
  public function compte(){
      $this->layout = 'connected';
      //ON définit le titre de la page
      $this->set('title_for_layout', 'SwissDirectories.PRO | Votre profil');
  }

  //Cette méthode va permettre à l'utilisateur de modifier son compte
  public function editer()
  {
      $this->Utilisateur->recursive = -1;
      $this->layout = 'connected';
      //ON définit le titre de la page
      $this->set('title_for_layout', 'SwissDirectories.PRO | Édition du profil');
      if($this->request->is('post')){
          //On récupère les données envoyer dans un tableau d
          $d = $this->request->data;
          //On insère l'id dans le tableau de donné afin que la méthode "save()" comprenne qu'il s'agit
          //d'une mise à jour et non pas d'une update
          $d['Utilisateur']['id'] = $this->Auth->User('id');
          //SI les nouveaux password sont vide
          if(empty($d['Utilisateur']['mot_de_passe']) OR empty($d['Utilisateur']['password2'])){
              //On supprime les cellules du tableau
              unset($d['Utilisateur']['mot_de_passe']);
              unset($d['Utilisateur']['password2']);
          }
          //On envoi les données au model afin qu'il fasse la validation et l'enregistrement
          if($this->Utilisateur->save($d)){
              //Récupérer les donneés de l'utilisateur
              $d = $this->Utilisateur->findById($d['Utilisateur']['id']);
              //Mise à jour des données dans la session
              $this->Session->write('Auth.User', $d['Utilisateur']);
              //Affichage du message de succès des modifications
              $this->Session->setFlash("Votre compte à bien été mis à jour", 'flash_notif', array('type' => 'success', 'titre' => 'Succès'));
              $this->redirect(array('action' => 'compte'));
          }else{
              //Affichage du message d'erreur des modifications
              $this->Session->setFlash("Merci de corriger vos erreurs", 'flash_notif', array('type' => 'error', 'titre' => 'Erreur'));
          }
      }
  }

  //Cette fonction va permettre d'effectuer la gestion des utilisateurs
  public function listing()
  {
      //On définit la récursivité à -1
      $this->Utilisateur->recursive = -1;
      //On définit le titre de la page
      $this->set('title_for_layout', 'SwissDirectories.PRO | Gestion des utilisateurs');
      //Déclaration du tableau $d
      $d = array();
      //création des conditions de base
      $conditions = array('Utilisateur.id !=' => $this->Auth->user('id'));
      $d['utilisateurs'] = $this->Utilisateur->find('all', array('conditions' => $conditions));
      $conditions['Utilisateur.is_active'] = 1;
      $d['nbr_total'] = count($d['utilisateurs']);
      $d['nbr_total_actif'] = $this->Utilisateur->find('count', array('conditions' => $conditions));
      //On envoi le résultat à la view
      $this->set($d);
  }

  //Cette focntion permet à un administrateur de voter pour la désactivation d'un utilisateur
  public function desactiver($id_utilisateur)
  {
      //Cette fonction n'est pas visible
      $this->autoRender = false;
      $this->Utilisateur->id = $id_utilisateur;
      //ON met la valeur de is_active à 0
      $this->Utilisateur->saveField('is_active', 0, array('callbacks' => false));
      $this->Session->setFlash("Le compte utilisateur a bien été désactivé", 'flash_notif', array('type' => 'success', 'titre' => 'Succès'));
      $this->redirect($this->referer());
  }

  //Cette méthode permet à un admin de réactiver un compte
  public function activer($id_utilisateur)
  {
      //Cette fonction n'est pas visible
      $this->autoRender = false;
      $this->Utilisateur->id = $id_utilisateur;
      //On met la valeur de is_active à 1
      $this->Utilisateur->saveField('is_active', 1, array('callbacks' => false));
      $this->Session->setFlash("Le compte utilisateur a bien été activé", 'flash_notif', array('type' => 'success', 'titre' => 'Succès'));
      $this->redirect($this->referer());
  }
}
?>
