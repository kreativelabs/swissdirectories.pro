<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="KreativeLabs.ch">
		<!-- Appel des feuilles de style css -->
		<?= $this->Html->css(array('../fonts/font-awesome', '../fonts/elegant-fonts', 'toastr')); ?>
		<!-- On charge la police google lato -->
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'>
		<?= $this->Html->css(array('../bootstrap/css/bootstrap', 'zabuto_calendar.min', 'owl.carousel', 'trackpad-scroll-emulator', 'jquery.nouislider.min', 'style')); ?>
		<title><?php echo $title_for_layout; ?></title>
		<?php echo '<meta name="description" content="'.$this->fetch('description').'" />'; ?>
		<?php
			echo $this->Html->meta('icon', 'icone.png');
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->Html->script(array('jquery-2.2.1.min', 'toastr'));
		?>

	</head>
	<?php //echo $this->Flash->render(); ?>
	<?php //echo $this->fetch('content'); ?>
	<?php //echo $this->element('sql_dump'); ?>

	<body class="homepage navigation-fixed">
		<?php echo $this->Session->flash(); ?>
		<div class="page-wrapper">
			<!-- Appel du menu -->
			<?= $this->element('menu'); ?>
			<!--end page-header-->
			<?php echo $this->fetch('content'); ?>
			<!--end page-content-->
			<?= $this->element('footer'); ?>
			<!--end page-footer-->
		</div>
		<!--end page-wrapper-->
		<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>
		<!-- Chargement des scripts javascript -->
		<?= $this->Html->script(array('jquery-migrate-1.2.1.min', '../bootstrap/js/bootstrap.min',
		'bootstrap-select.min')); ?>

		<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&libraries=places"></script>
		<?= $this->Html->script(array('richmarker-compiled', 'markerclusterer_packed', 'infobox',
		'jquery.validate.min', 'jquery.fitvids', 'moment', 'bootstrap-datetimepicker.min', 'icheck.min',
		'owl.carousel.min', 'jquery.nouislider.all.min', 'custom', 'maps')); ?>
		<script>
			var optimizedDatabaseLoading = 0;
			var _latitude = 46.806477;
			var _longitude = 7.161972;
			var element = "map-homepage";
			var markerTarget = "infobox"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
			var sidebarResultTarget = "sidebar"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
			var showMarkerLabels = true; // next to every marker will be a bubble with title
			var mapDefaultZoom = 14; // default zoom
			heroMap(_latitude,_longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom);
		</script>
		<?php echo $this->fetch('script'); ?>
	</body>
</html>
