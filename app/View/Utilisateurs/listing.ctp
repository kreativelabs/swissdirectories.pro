<div id="page_content_inner">
   <h3 class="heading_b uk-margin-bottom">Liste des utilisateurs</h3>
   <div class="md-card uk-margin-medium-bottom">
    <!-- Champ de filtrage -->
    <input id="filter" type="text" class="md-input" placeholder="Filtrer les utilisateurs" ></span>
      <div class="md-card-content">
         <div class="uk-overflow-container">
            <table class="uk-table">
               <thead>
                  <tr>
                     <th>Prénom</th>
                     <th>Nom</th>
                     <th>email</th>
                     <th>Date d'inscription</th>
                     <th>Date de dernière connexion</th>
                     <th>Actions</th>
                  </tr>
               </thead>
               <tbody  class="searchable">
                  <?php
                     foreach ($utilisateurs as $key => $value) {
                       echo '
                        <tr>
                          <td>'.$value['Utilisateur']['prenom'].'</td>
                          <td>'.$value['Utilisateur']['nom'].'</td>
                          <td>'.$value['Utilisateur']['email'].'</td>
                          <td>'.$value['Utilisateur']['date_inscription'].'</td>
                          <td>'.$value['Utilisateur']['date_connexion'].'</td>';
                          if($value['Utilisateur']['is_active'] == 1){
                            echo'<td>'.$this->Html->link('<i class="material-icons">&#xE872;</i>', array('controller' => 'Utilisateurs', 'action' => 'desactiver', $value['Utilisateur']['id']), array('title' => 'Désactiver le compte utilisateur', 'escape' => false),"Êtes vous sûr de vouloir désactiver cet utilisateur ?").'</td>';
                          }else{
                            echo'<td>'.$this->Html->link('<i class="material-icons">&#xE876;</i>', array('controller' => 'Utilisateurs', 'action' => 'activer', $value['Utilisateur']['id']), array('title' => 'Activer le compte utilisateur', 'escape' => false),"Êtes vous sûr de vouloir activer cet utilisateur ?").'</td>';
                          }
                          echo'
                        </tr>';
                     }
                     ?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
  $(document).on("click", ".change_statut", function () {
      var id = $(this).data('id');
      $('#id_commande').val(id);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function () {
    $('#filter').focus();
    (function ($) {
        $('#filter').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
        })
    }(jQuery));
});
</script>
<?php echo $this->element('modifier_statut');  ?>