<style type="text/css">
.error-message{
color:red;
}</style>
<?= $this->Html->css('../connected/assets/css/login_page.min', array('inline' => false)); ?>
<div class="login_page_wrapper">
   <br/>
   <div class="md-card" id="login_card">
      <!-- Formulaire de connexion -->
      <div class="md-card-content large-padding" id="login_form">
         <?= $this->Form->create(); ?>
            <div class="uk-form-row">
               <label for="login_username">Code</label>
               <?= $this->Form->input('code', array('class' => 'md-input', 'label' => false, 'div' => false, 'type' => 'text')); ?>
            </div>
            <div class="uk-margin-medium-top">
               <button class="md-btn md-btn-primary md-btn-block md-btn-large" type="submit">connexion</button>
            </div>
         <?= $this->Form->end(); ?>
      </div>
   </div>
</div>
<?= $this->Html->script(array('../connected/assets/js/pages/login.min', 'type_compte'),array('inline' => false)); ?>