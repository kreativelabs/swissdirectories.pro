<style>
  div.error-message{
    color:red;
  }
  .form-control{
    margin-top:10px;
  }
  .form-error{
    border:1px solid red!important;
  }
</style>
<div id="page-content">
    <div class="container">
        <!--end breadcrumb-->
        <div class="row">
            <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2">
                <section class="page-title">
                    <h1>Créer un compte</h1>
                </section
                <!--end page-title-->
                <section>
                    <?= $this->Form->create('Utilisateur', array('url' => array('controller' => 'Utilisateurs', 'action' => 'inscription'), 'class' => 'form inputs-underline')); ?>

                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="first_name">Prénom</label>
                                    <?php echo $this->Form->input('prenom', array('class' => 'form-control', 'id' => 'first_name', 'label' => false, 'div' => false, 'type' => 'text', 'placeholder' => 'Prénom')); ?>
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="last_name">Nom</label>
                                    <?php echo $this->Form->input('nom', array('class' => 'form-control', 'id' => 'last_name', 'label' => false, 'div' => false, 'type' => 'text', 'placeholder' => 'Nom')); ?>
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                        </div>
                        <!--enr row-->
                        <div class="form-group">
                            <label for="email">Email</label>
                            <?php echo $this->Form->input('email', array('class' => 'form-control', 'id' => 'email', 'label' => false, 'div' => false, 'type' => 'email', 'placeholder' => 'Email')); ?>
                        </div>
                        <!--end form-group-->
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <?php echo $this->Form->input('mot_de_passe', array('class' => 'form-control', 'id' => 'password', 'label' => false, 'div' => false, 'type' => 'password', 'placeholder' => 'Mot de passe')); ?>
                        </div>
                        <!--end form-group-->
                        <div class="form-group">
                            <label for="confirm_password">Confirmation du mot de passe</label>
                            <?= $this->Form->input('password2', array('class' => 'form-control', 'label' => false, 'div' => false, 'type' => 'password', 'id' => 'confirm_password')); ?>
                        </div>
                        <!--end form-group-->
                        <div class="form-group center">
                            <button type="submit" class="btn btn-primary width-100">Créer mon compte</button>
                        </div>

                        <p class="center">En cliquant sur le bouton “Créer mon compte” vous acceptez les conditions suivantes : <?php echo $this->Html->link('Conditions générales', array('controller' => 'Pages', 'action' => 'conditions_generales'), array('title' => 'Conditions générales', 'target' => '_blank')); ?> et <?php echo $this->Html->link('Politique de protection des données', array('controller' => 'Pages', 'action' => 'politique_donnees'), array('title' => 'Conditions générales', 'target' => '_blank')); ?>.</p>
                        <hr>
                    <?= $this->Form->end(); ?>
                </section>
            </div>
            <!--col-md-4-->
        </div>
        <!--end ro-->
    </div>
    <!--end container-->
</div>
