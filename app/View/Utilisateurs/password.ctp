<?= $this->Html->css('../connected/assets/css/login_page.min', array('inline' => false)); ?>
<div class="login_page_wrapper">
   <div class="uk-margin-top uk-text-center">
      <?= $this->Html->link($this->Html->image('logo.png', array('alt' => 'Logo SferaVino')), '/', array('escape' => false, 'title' => "Retourner à l'accueil")); ?>
   </div>
   <br/>
   <div class="md-card" id="login_card">
      <!-- Formulaire de connexion -->
      <div class="md-card-content large-padding" id="login_form">
        <h2>Réinitialisation de votre mot de passe</h2>
         <?= $this->Form->create('Utilisateur'); ?>
            <div class="uk-form-row">
               <label for="login_password">Nouveau mot de passe</label>
               <?= $this->Form->input('password', array('class' => 'md-input', 'label' => false, 'div' => false, 'type' => 'password')); ?>
            </div>
            <div class="uk-form-row">
               <label for="login_password">Confirmation du mot de passe</label>
               <?= $this->Form->input('password2', array('class' => 'md-input', 'label' => false, 'div' => false, 'type' => 'password')); ?>
            </div>
            <div class="uk-margin-medium-top">
               <?= $this->Form->end(array('label' =>"Reinitialisé", 'class' => 'md-btn md-btn-primary md-btn-block md-btn-large', 'div' => false)); ?>
            </div>
      </div>
   </div>
</div>
<?= $this->Html->script(array('../connected/assets/js/pages/login.min'),array('inline' => false)); ?>