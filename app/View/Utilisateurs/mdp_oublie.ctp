<?= $this->Html->css('../connected/assets/css/login_page.min', array('inline' => false)); ?>
<div class="login_page_wrapper">
   <div class="uk-margin-top uk-text-center">
      <?= $this->Html->link($this->Html->image('logo.png', array('alt' => 'Logo SferaVino')), '/', array('escape' => false, 'title' => "Retourner à l'accueil")); ?>
   </div>
   <br/>
   <div class="md-card" id="login_card">
      <div class="md-card-content large-padding" id="login_password_reset">
         <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
         <h2 class="heading_a uk-margin-large-bottom">Réinitialiser votre mot de passe</h2>
         <?= $this->Form->create('Utilisateur', array('url'=> array('controller' => 'Utilisateurs', 'action' => 'forgot'))); ?>
            <div class="uk-form-row">
               <label for="login_email_reset">Votre adresse email</label>
               <?= $this->Form->input('email', array('class' => 'md-input', 'label' => false, 'div' => false, 'type' => 'email', 'id' => 'login_email_reset')); ?>
            </div>
            <div class="uk-margin-medium-top">
               <button class="md-btn md-btn-primary md-btn-block" type="submit">Réinitialiser</button>
            </div>
         <?= $this->Form->end(); ?>
      </div>
   </div>
</div>
<?= $this->Html->script(array('../connected/assets/js/pages/login.min'),array('inline' => false)); ?>