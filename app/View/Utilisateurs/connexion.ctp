<style>
  div.error-message{
    color:red;
  }
  .form-control{
    margin-top:10px;
  }
  .form-error{
    border:1px solid red!important;
  }
</style>
<div id="page-content">
    <div class="container">
        <!--end breadcrumb-->
        <div class="row">
            <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                <section class="page-title">
                    <h1>Connexion</h1>
                </section>
                <!--end page-title-->
                <section>
                    <?= $this->Form->create('Utilisateur', array('url' => array('controller' => 'utilisateurs', 'action' => 'connexion'), 'class' => 'form inputs-underline')); ?>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <?php echo $this->Form->input('email', array('class' => 'form-control', 'id' => 'email', 'label' => false, 'div' => false, 'type' => 'email', 'placeholder' => 'Votre email')); ?>
                        </div>
                        <!--end form-group-->
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <?php echo $this->Form->input('mot_de_passe', array('class' => 'form-control', 'id' => 'password', 'label' => false, 'div' => false, 'type' => 'password', 'placeholder' => 'Votre mot de passe')); ?>
                        </div>
                        <div class="form-group center">
                            <button type="submit" class="btn btn-primary width-100">Connexion</button>
                        </div>
                        <!--end form-group-->
                    </form>
                    <?php echo $this->Form->end(); ?>

                    <hr>
                    <?php echo $this->Html->Link('Mot de passe oublié', array('controller' => 'utilisateurs', 'action' => 'mdp_oublie'), array('class' => '', 'style' => '', 'target' => '')); ?>
                </section>
            </div>
            <!--col-md-4-->
        </div>
        <!--end ro-->
    </div>
    <!--end container-->
</div>
