<div id="page_content_inner">
    <?php 
    //Récupération des données de l'utilisateur
    $user = Authcomponent::user();
    ?>
    <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
        <div class="uk-width-large-7-10">
            <div class="md-card">
                <div class="user_heading">
                    <div class="user_heading_avatar">
                        
                    </div>
                    <div class="user_heading_content">
                        <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate"><?= Authcomponent::user('prenom').' '.Authcomponent::user('nom'); ?></span><span class="sub-heading">Pseudo : <?= Authcomponent::user('pseudo'); ?></span></h2>
                    </div>
                    <?= $this->Html->link('<i class="material-icons">&#xE150;</i>', array('controller' => 'Utilisateurs', 'action' => 'editer'), array('escape' => false, 'class' => 'md-fab md-fab-small md-fab-accent', 'title' => 'Editer mon compte')); ?>
                </div>
                <div class="user_content">
                    <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                        <li class="uk-active"><a href="#">Mon compte</a></li>
                    </ul>
                    <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                        <li>
                            <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                <div class="uk-width-large-1-2">
                                    <h4 class="heading_c uk-margin-small-bottom">Coordonnées</h4>
                                    <ul class="md-list md-list-addon">
                                        <li>
                                            <div class="md-list-addon-element">
                                                <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                            </div>
                                            <div class="md-list-content">
                                                <span class="md-list-heading"><?= $user['email']; ?></span>
                                                <span class="uk-text-small uk-text-muted">Email</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                            </div>
                                            <div class="md-list-content">
                                                <span class="md-list-heading"><?php
                                                if(empty($user['telephone'])) {
                                                    echo '-';
                                                }else{
                                                    echo $user['telephone'];
                                                }
                                                ?></span>
                                                <span class="uk-text-small uk-text-muted">Téléphone</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="uk-width-large-1-2">
                                    <h4 class="heading_c uk-margin-small-bottom">Adresse</h4>
                                    <ul class="md-list">
                                        <li>
                                            <div class="md-list-content">
                                                <span class="md-list-heading">
                                                <?php
                                                if(empty($user['adresse'])){
                                                    echo '-';
                                                }else{
                                                    echo $user['adresse'];
                                                }
                                                ?></span>
                                                <span class="uk-text-small uk-text-muted">Rue</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-content">
                                                <span class="md-list-heading"><?php
                                                if(empty($user['code_postal'])){
                                                        echo '-';
                                                }else{
                                                    echo $user['code_postal'].' '.$user['ville'];
                                                }
                                                ?></span>
                                                <span class="uk-text-small uk-text-muted">Ville</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>