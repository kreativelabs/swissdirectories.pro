<?php 
//Récupération des données de l'utilisateur
$user = Authcomponent::user();
?>
<div id="page_content_inner">
    <?= $this->Form->create('Utilisateur', array('class' => 'uk-form-stacked', 'id' => 'edition')); ?>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-large-7-10">
                <div class="md-card">
                    <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">
                        <div class="user_heading_content">
                            <h2 class="heading_b"><span class="uk-text-truncate" id="user_edit_uname">Edition du profil</span></h2>
                        </div>
                        <div class="md-fab-wrapper">
                            <div class="md-fab md-fab-toolbar md-fab-small md-fab-accent">
                                <i class="material-icons enregistrer" title="Enregistrer">&#xE161;</i>
                            </div>
                        </div>
                    </div>
                    <div class="user_content">
                        <ul id="user_edit_tabs" class="uk-tab" data-uk-tab="{connect:'#user_edit_tabs_content'}">
                            <li class="uk-active"><a href="#">Mon compte</a></li>
                        </ul>
                        <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                            <li>
                                <div class="uk-margin-top">
                                    <h3 class="full_width_in_card heading_c">
                                        Vos informations
                                    </h3>
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-2">
                                            <label for="user_edit_uname_control">Nom</label>
                                            <?= $this->Form->input('nom', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['nom'])); ?>
                                        </div>
                                        <div class="uk-width-medium-1-2">
                                            <label for="user_edit_position_control">Prénom</label>
                                            <?= $this->Form->input('prenom', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['prenom'])); ?>
                                        </div>
                                        <div class="uk-width-medium-1-2">
                                            <label for="user_edit_position_control">Pseudo</label>
                                            <?= $this->Form->input('pseudo', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['pseudo'])); ?>
                                        </div>
                                        <div class="uk-width-medium-1-2">
                                            <label for="user_edit_position_control">Téléphone</label>
                                            <?= $this->Form->input('telephone', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['telephone'])); ?>
                                        </div>
                                        <div class="uk-width-medium-1-2">
                                            <label for="user_edit_position_control">Nouveau mot de passe</label>
                                            <?= $this->Form->input('password', array('type' => 'password', 'div' => false, 'label' => false, 'class' => 'md-input')); ?>
                                        </div>
                                        <div class="uk-width-medium-1-2">
                                            <label for="user_edit_position_control">Confirmation du mot de passe</label>
                                            <?= $this->Form->input('password2', array('type' => 'password', 'div' => false, 'label' => false, 'class' => 'md-input')); ?>
                                        </div>
                                    </div>
                                    <h3 class="full_width_in_card heading_c">
                                        Coordonnées
                                    </h3>
                                    <div class="uk-grid">
                                        <div class="uk-width-1-1">
                                            <div class="uk-grid uk-grid-width-1-1 uk-grid-width-large-1-2" data-uk-grid-margin>
                                                <div>
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon">
                                                            <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                        </span>
                                                        <label>Email</label>
                                                        <?= $this->Form->input('email', array('type' => 'email', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['email'])); ?>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="uk-input-group">
                                                        <span class="uk-input-group-addon">
                                                        </span>
                                                        <label>Adresse</label>
                                                        <?= $this->Form->input('adresse', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['adresse'])); ?>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="uk-input-group">
                                                    <span class="uk-input-group-addon">
                                                        </span>
                                                        <label>Complement d'adresse</label>
                                                        <?= $this->Form->input('complement_adresse', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['complement_adresse'])); ?>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="uk-input-group">
                                                    <span class="uk-input-group-addon">
                                                        </span>
                                                        <label>Code postale</label>
                                                        <?= $this->Form->input('code_postal', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['code_postal'])); ?>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="uk-input-group">
                                                    <span class="uk-input-group-addon">
                                                        </span>
                                                        <label>Ville</label>
                                                        <?= $this->Form->input('ville', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'md-input', 'value' => $user['ville'])); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Form->end(); ?>
</div>
<script type="text/javascript">
    $(document).on('click', ".enregistrer" , function() {
        $("#edition").submit();
    });
</script>