<footer id="page-footer">
  <div class="footer-wrapper">
    <div class="block">
      <div class="container">
        <div class="vertical-aligned-elements">
          <div class="element width-50">
            <p data-toggle="modal" data-target="#myModal">En utilisant et/ou consultant des informations ainsi que des documents disponibles
sur ce site (SwissDirectories.PRO), vous déclarez explicitement accepter les conditions suivantes : <?php echo $this->Html->link('Conditions générales', array('controller' => 'Pages', 'action' => 'conditions_generales'), array('title' => 'Conditions générales', 'target' => '_blank')). " et ". $this->Html->link('Politique de protection des données', array('controller' => 'Pages', 'action' => 'politique_donnees'), array('title' => 'Conditions générales', 'target' => '_blank')); ?>.</p>
          </div>
          <div class="element width-50 text-align-right">
            <a href="#" class="circle-icon"><i class="social_twitter"></i></a>
            <a href="#" class="circle-icon"><i class="social_facebook"></i></a>
            <a href="#" class="circle-icon"><i class="social_youtube"></i></a>
          </div>
        </div>
        <div class="background-wrapper">
          <div class="bg-transfer opacity-50">
            <?php echo $this->Html->image('footer-bg.png', array('class' => '', 'alt' => ' ')); ?>
          </div>
        </div>
        <!--end background-wrapper-->
      </div>
    </div>
    <div class="footer-navigation">
      <div class="container">
        <div class="vertical-aligned-elements">
          <div class="element width-50">© Copyright <script>document.write(new Date().getFullYear());</script>   |   All Rights Reserved   |   SWISSDIRECTORIES.PRO | POWERED BY <a href="KreativeLabs.ch" target="_blank">KreativeLabs.ch</a></div>
          <div class="element width-50 text-align-right">
            <a href="index.html">Home</a>
            <a href="listing-grid-right-sidebar.html">Listings</a>
            <a href="submit.html">Submit Item</a>
            <a href="contact.html">Contact</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
