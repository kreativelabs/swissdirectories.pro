<header id="page-header">
  <nav>
    <!-- Bloc logo -->
    <div class="left">
      <?= $this->Html->image('logo/logo-texte.png',
      array('alt' => 'Logo SwissDirectories.Pro', 'url' => array('controller' => 'entreprises', 'action' => 'accueil'),
    'class' => 'brand', 'style' => 'max-height:50px !important;')); ?>
    </div>
    <!--end left-->
    <div class="right">
      <div class="primary-nav has-mega-menu">
        <ul class="navigation">
          <li>
            <?php echo $this->Html->Link('Accueil', array('controller' => 'entreprises', 'action' => 'accueil'), array('class' => '', 'style' => '', 'target' => '')); ?>
          </li>
          <li class="has-child">
            <a href="#">Nos services pour PME</a>
            <div class="wrapper">
              <div id="nav-homepages" class="nav-wrapper">
                <ul>
                  <li class="has-child" style="width:200px;">
                    <?php echo $this->Html->Link('Agence web & Marketing', array('controller' => 'services', 'action' => 'web_marketing'), array('class' => '', 'style' => '', 'target' => '')); ?>
                    <div id="nav-grid-listing" class="nav-wrapper">
                      <ul>
                        <li><?php echo $this->Html->Link('Design & Graphisme', array('controller' => 'services', 'action' => 'web_marketing', '#' => 'design'), array('class' => '', 'style' => '', 'target' => '')); ?></li>
                        <li><?php echo $this->Html->Link('Développement Web', array('controller' => 'services', 'action' => 'web_marketing', '#' => 'dev-web'), array('class' => '', 'style' => '', 'target' => '')); ?></li>
                        <li><?php echo $this->Html->Link('Consulting SEO', array('controller' => 'services', 'action' => 'web_marketing', '#' => 'seo'), array('class' => '', 'style' => '', 'target' => '')); ?></li>
                        <li><?php echo $this->Html->Link('Marketing Digital', array('controller' => 'services', 'action' => 'web_marketing', '#' => 'digital-marketing'), array('class' => '', 'style' => '', 'target' => '')); ?></li>

                      </ul>
                    </div>
                  </li>

                </ul>
              </div>
            </div>
          </li>

          <!--
          Les villes à la unes avec les meilleures entreprsies par domaine (jusqu'à 5 domaines)
          <li class="has-child">
            <a href="#nav-locations">Villes #trendy</a>
            <div class="wrapper">
              <div id="nav-locations" class="nav-wrapper">
                <ul>
                  <li class="has-child">
                    <a href="#nav-locations-new-york">Genève</a>
                    <div class="nav-wrapper" id="nav-locations-new-york">
                      <ul>
                        <li class="has-child">
                          <a href="#nav-4">Restaurants</a>
                          <div class="nav-wrapper" id="nav-4">
                            <ul>
                              <li><a href="#">1</a></li>
                              <li><a href="#">2</a></li>
                              <li><a href="#">3</a></li>
                              <li><a href="#">4</a></li>
                            </ul>
                          </div>
                        </li>
                        <li><a href="#">Bijoutiers</a></li>
                        <li><a href="#">Fleuristes</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="has-child">
                    <a href="#nav-5">London</a>
                    <div class="nav-wrapper" id="nav-5">
                      <ul>
                        <li><a href="#">Abbey Wood</a></li>
                        <li><a href="#">Bayswater</a></li>
                        <li><a href="#">Forestdale</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="has-child">
                    <a href="#nav-6">Paris</a>
                    <div class="nav-wrapper" id="nav-6">
                      <ul>
                        <li><a href="#">Louvre</a></li>
                        <li><a href="#">Bourse</a></li>
                        <li><a href="#">Marais</a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </li>
          -->
          <li><?php echo $this->Html->Link('Blog', array('controller' => 'Articles', 'action' => 'index'), array('class' => '', 'style' => '', 'target' => '')); ?></li>
          <li><?php echo $this->Html->Link('Contactez-nous', array('controller' => 'Pages', 'action' => 'contact'), array('class' => '', 'style' => '', 'target' => '')); ?></li>
        </ul>
        <!--end navigation-->
      </div>
      <!--end primary-nav-->
      <div class="secondary-nav">
        <?php echo $this->Html->Link('Connexion', array('controller' => 'utilisateurs', 'action' => 'connexion'), array('style' => '', 'target' => '')); ?>
        <?php echo $this->Html->Link('Inscription', array('controller' => 'utilisateurs', 'action' => 'inscription'), array('class' => 'promoted', 'style' => '', 'target' => '')); ?>
      </div>
      <!--end secondary-nav-->
      <a href="#" class="btn btn-primary btn-small btn-rounded icon shadow add-listing" data-modal-external-file="modal_submit.php" data-target="modal-submit"><i class="fa fa-plus"></i><span>Ajouter mon Entreprise</span></a>
      <div class="nav-btn">
        <i></i>
        <i></i>
        <i></i>
      </div>
      <!--end nav-btn-->
    </div>
    <!--end right-->
  </nav>
  <!--end nav-->
</header>
