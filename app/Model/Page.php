<?php
class Page extends AppModel{
    //Ce model n'utilise pas de table
    public $useTable = false;
	//Règle de validaiton pour le model
    public $validate = array(
        'nom' => array(
            'rule' => 'notBlank',
            'message' => 'Ce champs ne peut pas être vide'
        ),
        'prenom' => array(
            'rule' => 'notBlank',
            'message' => 'Ce champs ne peut pas être vide'
        ),
        'email' => array(
            'empty' => array(
                'rule' => 'notBlank',
                'message' => 'Ce champs ne peut pas être vide'
            ),
            'mail' => array(
                'rule' => 'email',
                'message' => 'Veuillez entrer une adresse mail'
            )
        ),
        'message' => array(
            'rule' => 'notBlank',
            'message' => "Ce champs ne peut pas être vide"
        ),
        'sujet' => array(
            'rule' => 'notBlank',
            'message' => "Ce champs ne peut pas être vide"
        ),
        'telephone' => array(
            array(
                'rule' => '/^[0-9]{8,15}$/i',
                'allowEmpty' => true,
                'message' => 'Veuillez entrer un numéro de téléphone valide, exemple 079xxxxxxx ou 022xxxxxxx'),
        )
    );

    //Cette fonction va envoyer les mails du site, elle reçoit en paramètre les données de l'email dans $data et le type d'email dans $type
    public function send_mail($data, $type)
    {
        //Déclaration de l'objet CakeEmail
        App::uses('CakeEmail', 'Network/Email');
        //Définition de l'adresse email d'envoi
        $mail = 'default';
        if($_SERVER['SERVER_NAME'] == 'localhost'){
        
            $mail = 'gmail';
        }
        //On affecte les arguments au composant mail
        $CakeEmail = new CakeEmail($mail);
        $CakeEmail->to($data['destinataire']);
        $CakeEmail->subject($data['sujet']);
        $CakeEmail->template($data['template']);
        $CakeEmail->emailFormat('html');
        if($type == 1){
            //SI le type de mail est 1 (inscription d'un nouvel utilisateur), on va envoyer les viewvars suivante au template d'email
            $CakeEmail->viewVars(array('token' => $data['token'], 'id' => $data['id']));
            
        }elseif ($type == 2) {
            //On log les datas reçu dans le journal "commande.log"
            //CakeLog::write('info', print_r($data,true), array('commande'));
            //SI le type de mail est 2 (nouvelle commande d'un utilisateur non conncecté), on va envoyer les viewvars suivante au template d'email
            $CakeEmail->viewVars(
                array(
                    'commande' => $data['commande'],
                    'titre' => $data['titre'],
                    'panier' => $data['panier']
                )
            );
        }elseif($type == 3){
            //On change le sujet pour l'adapter à la page de contact
            $CakeEmail->subject('Message de la page contact : '.$data['sujet']);
            $CakeEmail->viewVars(
                $data
            );
        }
        //Envoi de l'email
        if($CakeEmail->send()){
            //Si l'email est bien envoyé, on renvoit true à la méthode appelante
            return true;
        }else{
            //Sinon on renvoit false à la méthode appelante
            return false;
        }
    }
}
?>