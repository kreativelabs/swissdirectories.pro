<?php
class Contact extends AppModel {
    public $useTable = false;
    //Cette fonction va envoyer les mails du site, elle reçoit en paramètre les données de l'email dans $data et le type d'email dans $type
    public function send_mail($data, $type)
    {
    	//Déclaration de l'objet CakeEmail
        App::uses('CakeEmail', 'Network/Email');
        //Définition de l'adresse email d'envoi
        $mail = 'default';
        if($_SERVER['SERVER_NAME'] == 'localhost'){

            $mail = 'gmail';
        }
        //On affecte les arguments au composant mail
        $CakeEmail = new CakeEmail($mail);
        $CakeEmail->to($data['destinataire']);
        $CakeEmail->subject($data['sujet']);
        $CakeEmail->template($data['template']);
        $CakeEmail->emailFormat('html');
    	if($type == 1){
            //Si le type de mail est 1 (inscription d'un nouvel utilisateur), on va envoyer les viewvars suivante au template d'email
            $CakeEmail->viewVars(array('token' => $data['token'], 'id' => $data['id']));
    	}
      //Envoi de l'email
      if($CakeEmail->send()){
          //Si l'email est bien envoyé, on renvoit true à la méthode appelante
          return true;
      }else{
          //Sinon on renvoit false à la méthode appelante
          return false;
      }
    }
}
?>
