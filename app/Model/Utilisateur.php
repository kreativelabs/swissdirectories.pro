<?php
class Utilisateur extends AppModel{
  //Définitino du comportement containable
  public $actsAs = array('Containable');
  /*//On définit les relations avec les autres models
  public $belongsTo = array(
      'type_utilisateur' => array(
          'className' => 'TypeUtilisateur',
          'foreignKey' => 'id_type_utilisateur'
      )
  );

  public $hasMany = array(
      'mes_commandes' => array(
          'className' => 'Commande',
          'foreignKey' => 'id_client'
      ),
      'mailbox' => array(
          'className' => 'Message',
          'foreignKey' => 'id_recepteur'
      )
  );*/

  public $validate = array(
      'email' => array(
          'empty' => array(
              'rule' => 'notBlank',
              'message' => 'Veuillez indiquer votre adresse mail'
          ),
          'mail' => array(
              'rule' => 'email',
              'message' => 'Veuillez entrer une adresse mail'
          ),
          'uniq' => array(
              'rule' => 'isUnique',
              'message' => "Cette adresse mail est déjà utilisé"
          )
      ),
      'pseudo' => array(
          'empty' =>  array(
              'rule' => 'notBlank',
              'message' => 'Veuillez indiquer un pseudo'
          ),
          'uniq' => array(
              'rule' => 'isUnique',
              'message' => "Ce nom d'utilisateur est déjà pris est déjà pris"
          )
      ),
      'nom' => array(
          'rule' => 'notBlank',
          'message' => 'Veuillez indiquer votre nom'
      ),
      'prenom' => array(
          'rule' => 'notBlank',
          'message' => 'Veuillez indiquer votre prénom'
      ),
      'telephone' => array(
          'tel' => array(
              'rule' => '/^([+]{1}|[0]{1})[0-9]{2}\s*([0-9]{3}|[0-9]{2}\s*[0-9]{3}|\([0]{1}\)\s*[0-9]{2}\s*[0-9]{3})\s*[0-9]{2}\s*[0-9]{2}\s*$/ix',
              'allowEmpty' => true,
              'message' => 'Veuillez entrer un numéro de téléphone valide. Exemple 079 xxx xx xx'
          )
      ),
      'mot_de_passe' => array(
          'length' => array(
              'rule'      => array('between', 6, 40),
              'message'   => 'Votre mot de passe doit être compris entre 6 et 40 caractères',
          ),
          'empty' => array(
              'rule' => 'notBlank',
              'message' => 'Veuillez indiquer votre mot de passe'
          )
      ),
      'password2' => array(
          'length' => array(
              'rule'      => array('between', 6, 40),
              'message'   => 'Votre mot de passe doit être compris entre 6 et 40 caractères',
          ),
          'compare' => array(
              'rule' => 'identicalFields',
              'required' => false,
              'message' => "Ce mot de passe n'est pas identique à l'original"
          ),
          'empty' => array(
              'rule' => 'notBlank',
              'message' => 'Veuillez indiquer votre mot de passe'
          )
      ),
      'code_postal' => array(
          'numeric' => array(
              'rule' => 'numeric',
              'message' => 'Veuillez indiquer un code postal valide',
              'allowEmpty' => true
          ),
          'length' => array(
              'rule'      => array('between', 4, 5),
              'message'   => "Veuillez indiquer un code postal valide",
              'allowEmpty' => true
          )
      )
  );


  public function identicalFields($check, $limit){
      $field = key($check);
      return $check[$field] == $this->data['Utilisateur']['mot_de_passe'];
  }

  public function isJpg($check, $limit){
      $field = key($check);
      $filename = $check[$field]['name'];
      if(empty($filename)){
          return true;
      }
      $info = pathinfo($filename);
      return strtolower($info['extension']) == 'jpg';
  }

  //Va récupérer les donneés de l'utilisateur pour le dashboard
  public function get_user()
  {
      //Récupération de l'id de l'utilisateur
      $uid = CakeSession::read("Auth.User.id");
      //ON renvoi les données de l'utilisateur
      return $this->findById($uid);
  }

  function beforeSave($options = array()) {
      //Si le mot de passe n'est pas vide, alors on le hash afin de l'enregistrer sous sa forme crypté dans la BDD
      if (!empty($this->data['Utilisateur']['mot_de_passe'])) {
          $this->data['Utilisateur']['mot_de_passe'] = Security::hash($this->data['Utilisateur']['mot_de_passe'], null, true);
      }
  }
}
?>
